package com.example.conor.popularmovies;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.conor.popularmovies.conditions.getPosterResults;

public class MainActivity extends AppCompatActivity {
    private MovieAdapter movieAdapter;
    private GridView gridView;
    private String type;
    private String URL = "http://api.themoviedb.org/3/movie/";
    private String Api_Key = "?api_key={api_key}";
    private String Error_message = "You are not connected"; //When you getString(R.string.Error_Message) app crashes, why?
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        type = "popular";
        processMovies movie = new processMovies(URL + type + Api_Key);
        if(isNetworkAvailable()) {
            movie.execute();
        }
        else{
            Toast.makeText(this, Error_message, Toast.LENGTH_SHORT).show();

        }
        gridView = (GridView) findViewById(R.id.Movies_grid);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, Detail.class);
                intent.putExtra(Intent.EXTRA_TEXT, movieAdapter.getMovie(i));
                startActivity(intent);
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh, menu);
        getMenuInflater().inflate(R.menu.swap, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int idnumb = item.getItemId();
        if (idnumb == R.id.top_rated) {
            type = "top_rated";
            processMovies movie = new processMovies(URL + type + Api_Key);
            if(isNetworkAvailable()) {
                movie.execute();
            }
            else{
                Toast.makeText(this, Error_message, Toast.LENGTH_SHORT).show();

            }
        }
        if (idnumb == R.id.popular) {
            type = "popular";
            processMovies movie = new processMovies(URL + type + Api_Key);
            if(isNetworkAvailable()) {
                movie.execute();
            }
            else{
                Toast.makeText(this, Error_message, Toast.LENGTH_SHORT).show();

            }
        }
        if (idnumb== R.id.Refresh){
            processMovies movie = new processMovies(URL + type + Api_Key);
            if(isNetworkAvailable()) {
                movie.execute();
            }
            else{
                Toast.makeText(this, Error_message, Toast.LENGTH_SHORT).show();

            }
        }
        return true;
    }

    public class processMovies extends getPosterResults {
        public processMovies(String url) {
            super(url);
        }

        public void execute() {
            super.execute();
            ProcessMovie processMovie = new ProcessMovie();
            processMovie.execute();
        }

        public class ProcessMovie extends DownloadJson {

            @Override
            protected void onPostExecute(String webinfo) {
                super.onPostExecute(webinfo);
                movieAdapter = new MovieAdapter(MainActivity.this, R.layout.movieitem, getMovies());
                gridView.setAdapter(movieAdapter);
            }
        }
    }
}

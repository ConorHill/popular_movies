package com.example.conor.popularmovies;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.conor.popularmovies.conditions.Movie;
import com.squareup.picasso.Picasso;

public class Detail extends AppCompatActivity {
    private String BASE_URL = "http://image.tmdb.org/t/p/w185";
    private Movie movie;
    private Context context;
    private ImageView movie_poster;
    private TextView title;
    private TextView overview;
    private TextView release_date;
    private TextView vote_average;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent intent = getIntent();
        if(intent != null){
            if(intent.hasExtra(Intent.EXTRA_TEXT)){
                movie = (Movie) intent.getSerializableExtra(Intent.EXTRA_TEXT);
            }
        }
        title = (TextView) findViewById(R.id.movie_title);
        overview = (TextView) findViewById(R.id.movie_overview);
        release_date = (TextView) findViewById(R.id.movie_release_date);
        vote_average = (TextView) findViewById(R.id.movie_vote_average);
        movie_poster = (ImageView) findViewById(R.id.movie_poster);

        title.append(movie.getOriginal_title());
        overview.append("\n"+movie.getOverview());
        release_date.append(movie.getRelease_date());
        vote_average.append(movie.getVote_average()+"/10");
        Picasso.with(context).load(BASE_URL+movie.getPoster_path()).into(movie_poster);
    }
}

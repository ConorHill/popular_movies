package com.example.conor.popularmovies.conditions;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by conor on 07/02/2017.
 */

public class DownloadData {

    private String data;
    private String JsonUrl;

    public DownloadData(String url) {
        JsonUrl = url;
    }

    public String getData() {
        return data;
    }

    public void execute() {
        DownloadJsonData downloadJsonData = new DownloadJsonData();
        downloadJsonData.execute(JsonUrl);
    }

    public class DownloadJsonData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... parms) {
            String pageResults = null;
            if (parms == null)
                return null;
                try {
                    URL url = new URL(parms[0]);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("GET");
                    connection.connect();
                    InputStream is = connection.getInputStream();
                    StringBuffer buffer = new StringBuffer();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line + "\n");
                    }
                    return buffer.toString();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            return pageResults;
        }

        @Override
        protected void onPostExecute(String webinfo) {
            if (webinfo != null && !webinfo.equals("")) {
                data = webinfo;
            }
        }
    }
}

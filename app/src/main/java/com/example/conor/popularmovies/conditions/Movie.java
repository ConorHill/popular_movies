package com.example.conor.popularmovies.conditions;

import java.io.Serializable;

/**
 * Created by conor on 07/02/2017.
 */

public class Movie implements Serializable {
    private String original_title;
    private String poster_path;
    private String overview;
    private String release_date;
    private String vote_average;

    public Movie(String original_title, String poster_path, String overview, String release_date, String vote_average) {
        this.original_title = original_title;
        this.poster_path = poster_path;
        this.overview = overview;
        this.release_date = release_date;
        this.vote_average = vote_average;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public String getOverview() {
        return overview;
    }

    public String getRelease_date() {
        return release_date;
    }

    public String getVote_average() {
        return vote_average;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "original_title='" + original_title + '\'' +
                ", poster_path='" + poster_path + '\'' +
                ", overview='" + overview + '\'' +
                ", release_date='" + release_date + '\'' +
                ", vote_average='" + vote_average + '\'' +
                '}';
    }
}

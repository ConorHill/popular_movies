package com.example.conor.popularmovies;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.example.conor.popularmovies.conditions.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by conor on 08/02/2017.
 */

public class MovieAdapter extends ArrayAdapter<Movie> {
    private static final String TAG = MovieAdapter.class.getSimpleName();
    private List<Movie> movies;
    private String IMG_URL = "http://image.tmdb.org/t/p/w185";
    Context context;
    int resource;

    public MovieAdapter(Context context, int resource, List<Movie> objects) {
        super(context, resource, objects);
        this.movies = objects;
        this.context = context;
        this.resource = resource;
    }

    @Override
    public int getCount() {
        if(movies.size()==0){
            return 0;
        }
        else{
            return movies.size();
        }
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RecordHolder holder;
        LayoutInflater inflater = ((Activity) context ).getLayoutInflater();

        if(convertView==null){
            convertView = inflater.inflate(R.layout.movieitem, parent, false);
            holder = new RecordHolder();
            holder.Poster = (ImageView) convertView.findViewById(R.id.MoiveImage);
            convertView.setTag(holder);
        }
        else{
            holder = (RecordHolder) convertView.getTag();
        }
        Movie movie = movies.get(position);
        Picasso.with(context).load(IMG_URL+movie.getPoster_path()).into(holder.Poster);

        return convertView;
    }

    static class RecordHolder{
        ImageView Poster;
    }

    public Movie getMovie(int position){
        return(null != movies ? movies.get(position) : null);
    }
}

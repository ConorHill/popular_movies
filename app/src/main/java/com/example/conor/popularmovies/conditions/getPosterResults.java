package com.example.conor.popularmovies.conditions;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by conor on 07/02/2017.
 */

public class getPosterResults extends DownloadData {


    private String INFO_URL;
    public List<Movie> Movies;
    public getPosterResults(String url){
        super(url);
        INFO_URL=url;
        Movies = new ArrayList<Movie>();
    }

    public void execute(){
        DownloadJson downloadJson = new DownloadJson();
        downloadJson.execute(INFO_URL);
    }

    public void ProcessJson(){
        final String MOVIE_RESULTS="results";
        final String MOVIE_TITLE="original_title";
        final String MOVIE_OVERVIEW="overview";
        final String MOVIE_RELEASE="release_date";
        final String MOVIE_VOTE="vote_average";
        final String MOVIE_IMAGE="poster_path";
        try{
            JSONObject object = new JSONObject(getData());
            JSONArray MovieArray = object.getJSONArray(MOVIE_RESULTS);
            for(int i=0; i<MovieArray.length(); i++){
                JSONObject jsonMovie = MovieArray.getJSONObject(i);
                String title = jsonMovie.getString(MOVIE_TITLE);
                String overview = jsonMovie.getString(MOVIE_OVERVIEW);
                String release_date = jsonMovie.getString(MOVIE_RELEASE);
                String vote_average = jsonMovie.getString(MOVIE_VOTE);
                String Image = jsonMovie.getString(MOVIE_IMAGE);

                Movie movieobject= new Movie(title, Image, overview, release_date, vote_average);
                this.Movies.add(movieobject);
            }
        }catch(JSONException e){
            e.printStackTrace();
        }
            for(Movie one : Movies)
            Log.v("DownloadData", one.toString());
    }
    public List<Movie> getMovies(){
        return Movies;
    }

    public class DownloadJson extends DownloadData.DownloadJsonData{
        @Override
        protected String doInBackground(String... parms) {
            return super.doInBackground(INFO_URL);
        }

        @Override
        protected void onPostExecute(String webinfo) {
            super.onPostExecute(webinfo);
            ProcessJson();
        }
    }
}
